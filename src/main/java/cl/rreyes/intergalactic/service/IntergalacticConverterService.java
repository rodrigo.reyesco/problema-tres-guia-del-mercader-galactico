package cl.rreyes.intergalactic.service;

import cl.rreyes.intergalactic.model.IntergalacticUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.StringTokenizer;

@Service
public class IntergalacticConverterService {

    @Autowired
    RomanConverterService romanConverterService;
    private static final HashMap<String, String> intergalacticNumberMap = new HashMap<String, String>();
    private static final int SILVER = 17;
    private static final int GOLD = 14450;
    private static final Double IRON = 195.5;

    public IntergalacticConverterService() {
        intergalacticNumberMap.put("GLOB", "I");
        intergalacticNumberMap.put("PROK", "V");
        intergalacticNumberMap.put("PISH", "X");
        intergalacticNumberMap.put("TEGJ", "L");
        intergalacticNumberMap.put("GLOB", "I");
        intergalacticNumberMap.put("SILVER", "");
        intergalacticNumberMap.put("GOLD", "");
        intergalacticNumberMap.put("IRON", "");

    }

    public IntergalacticUnit intergalacticToNumberConverter(String intergalactic) {

        StringTokenizer tokenizer = new StringTokenizer(intergalactic);
        String[] intergalacticArray = new String[tokenizer.countTokens()];
        int index = 0;
        String intergalacticNumberString = "";
        String romanNumber = "";
        int intergalacticNumber = 0;
        while (tokenizer.hasMoreTokens()) {
            intergalacticNumberString = intergalacticArray[index++] = tokenizer.nextToken();
            if (intergalacticNumberString != null && !intergalacticNumberString.trim().isEmpty()) {
                intergalacticNumberString = intergalacticNumberString.toUpperCase();
                romanNumber = romanNumber.concat(intergalacticNumberMap.get(intergalacticNumberString));
            }
        }
        intergalacticNumber = (romanConverterService.romanToNumberConverter(romanNumber));

        if (intergalactic != null && !intergalactic.trim().isEmpty()) {
            if (intergalactic.toUpperCase().contains("SILVER")) {
                intergalacticNumber = (intergalacticNumber * this.SILVER);
            } else if (intergalactic.toUpperCase().contains("GOLD")) {
                intergalacticNumber = (intergalacticNumber * this.GOLD);
            } else if (intergalactic.toUpperCase().contains("IRON")) {
                intergalacticNumber = Double.valueOf((Double.valueOf(intergalacticNumber).doubleValue() * this.IRON)).intValue();
            } else {
                intergalacticNumber = intergalacticNumber;
            }
        }

        IntergalacticUnit unit = new IntergalacticUnit(intergalactic.trim(), intergalacticNumber, intergalactic.trim().concat(" is ").concat("" + intergalacticNumber+" Credits"));
        return unit;
    }
}
