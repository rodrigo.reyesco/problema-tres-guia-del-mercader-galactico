package cl.rreyes.intergalactic.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class RomanConverterService {
    private static final HashMap<Character, Integer> romanNumberMap = new HashMap<Character, Integer>();

    public RomanConverterService() {
        romanNumberMap.put('I',1);
        romanNumberMap.put('V',5);
        romanNumberMap.put('X',10);
        romanNumberMap.put('L',50);
        romanNumberMap.put('C',100);
        romanNumberMap.put('D',500);
        romanNumberMap.put('M',1000);
    }

    public Integer romanToNumberConverter(final String roman) {
        int number = 0;
        String romanNumber = roman.toUpperCase();
        romanNumber = romanNumber.replace("IV","IIII");
        romanNumber = romanNumber.replace("IX","VIIII");
        romanNumber = romanNumber.replace("XL","XXXX");
        romanNumber = romanNumber.replace("XC","LXXXX");
        romanNumber = romanNumber.replace("CD","CCCC");
        romanNumber = romanNumber.replace("CM","DCCCC");

        for (int i = 0; i < romanNumber.length(); i++) {
            number = number + (romanNumberMap.get(romanNumber.charAt(i)));
        }
        return number;
    }
}
