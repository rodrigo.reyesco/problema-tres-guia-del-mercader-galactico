package cl.rreyes.intergalactic.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Represents an Intergalactic number and its exchange rate to Arabic numerals and Response message")
public class IntergalacticUnit {

    @Schema(name = "intergalactic", description = "Intergalactic number")
    String intergalactic;
    @Schema(name = "arabic", description = "Exchange rate in Arabic number")
    Integer arabic;
    @Schema(name = "message", description = "Response message")
    String message;

    public IntergalacticUnit() {
    }

    public IntergalacticUnit(String intergalactic, Integer arabic, String message) {
        this.intergalactic = intergalactic;
        this.arabic = arabic;
        this.message = message;
    }

    public String getIntergalactic() {
        return intergalactic;
    }

    public void setIntergalactic(String intergalactic) {
        this.intergalactic = intergalactic;
    }

    public Integer getArabic() {
        return arabic;
    }

    public void setArabic(Integer arabic) {
        this.arabic = arabic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
