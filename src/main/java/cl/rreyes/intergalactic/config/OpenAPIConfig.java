package cl.rreyes.intergalactic.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;


@Configuration
public class OpenAPIConfig {

    @Value("${guia.mercader.galactico.openapi.dev-url}")
    private String devUrl;

    @Value("${guia.mercader.galactico.openapi.prod-url}")
    private String prodUrl;

    @Bean
    public OpenAPI myOpenAPI() {
        Server prodServer = new Server();
        prodServer.setUrl(prodUrl);
        prodServer.setDescription("Server URL in Production environment");

        Server devServer = new Server();
        devServer.setUrl(devUrl);
        devServer.setDescription("Server URL in Development environment");

        Contact contact = new Contact();
        contact.setEmail("rodrigo.reyesco@gmail.com");
        contact.setName("Rodrigo Reyes");
        contact.setUrl("https://www.rodrigo-reyes.com");

        License mitLicense = new License().name("MIT License").url("https://choosealicense.com/licenses/mit/");

        Info info = new Info()
                .title("Problema Tres, Guía del Mercader Galáctico API")
                .version("1.0")
                .contact(contact)
                .description("This API exposes endpoints to manage methods to convert Intergalactics Units.").termsOfService("https://www.rodrigo-reyes.com/terms")
                .license(mitLicense);

        return new OpenAPI().info(info).servers(List.of(prodServer, devServer));
    }
}
