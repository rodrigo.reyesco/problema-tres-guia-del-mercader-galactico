package cl.rreyes.intergalactic.controller;

import cl.rreyes.intergalactic.model.IntergalacticUnit;
import cl.rreyes.intergalactic.service.IntergalacticConverterService;
import cl.rreyes.intergalactic.service.RomanConverterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@Tag(name = "Intergalactic Converter", description = "Intergalactic Converter APIs")
public class IntergalacticConverterController {

    @Autowired
    RomanConverterService romanConverterService;

    @Autowired
    IntergalacticConverterService intergalacticConverterService;


    @GetMapping("/intergalactic-to-arabic/{intergalactic}")
    @Operation(description = "Convert Intergalactic Units to Arabic numerals")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad Request"),
            @ApiResponse(responseCode = "404", description = "Resource not found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public ResponseEntity<IntergalacticUnit> convertIntergalacticToArabic(@PathVariable(value = "intergalactic", required = true, name = "intergalactic") @Parameter(name = "intergalactic", example = "pish tegj glob glob") String intergalactic) {
        try {
            if (intergalactic == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<IntergalacticUnit>(intergalacticConverterService.intergalacticToNumberConverter(intergalactic), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<IntergalacticUnit>(new IntergalacticUnit(intergalactic, null, "I have no idea what you are talking about"), HttpStatus.BAD_REQUEST);
        }
    }

}
