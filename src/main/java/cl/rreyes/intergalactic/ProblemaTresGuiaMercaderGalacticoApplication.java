package cl.rreyes.intergalactic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProblemaTresGuiaMercaderGalacticoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProblemaTresGuiaMercaderGalacticoApplication.class, args);
	}

}
